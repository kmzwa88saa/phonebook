require 'rails_helper'

describe Phone do
  it 'has a valid factory' do
    expect(build(:phone)).to be_valid
  end

  it 'is invalid without a phone' do
    expect(build(:phone, phone: nil)).to_not be_valid
  end
end
