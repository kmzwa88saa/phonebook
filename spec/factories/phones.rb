FactoryGirl.define do
  factory :phone do
    phone         { Faker::Number.number(10) }
    user_id       { Faker::Number.between(1, 21) }
  end
end
