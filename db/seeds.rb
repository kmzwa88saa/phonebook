User.create!(name:  "Januar Fonti",
             email: "admin@admin.com",
             password:              "password",
             password_confirmation: "password",
             role: "admin" )

20.times do |n|
  name  = Faker::Name.name
  email = "#{n+1}@user.com"
  password = "password"
  role = "user"
  User.create!(name:  name,
               email: email,
               password:              password,
               password_confirmation: password,
               role: role)
end
