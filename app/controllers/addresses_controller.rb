class AddressesController < ApplicationController
  load_and_authorize_resource
  def index
    @addresses = Address.where(user_id: current_user.id)
  end

  def show

  end

  def new
    @address = Address.new
  end

  def create
    @address = Address.create(address_params)
    @address.user_id = current_user.id
    if @address.save
      redirect_to addresses_path
    else
      render 'new'
    end
  end

  def show
  end

  def edit
    @address = Address.find(params[:id])
  end

  def update
    @address = Address.find(params[:id])
    if @address.update_attributes(address_params)
      redirect_to addresses_path
    else
      render 'edit'
    end
  end

  def destroy
    @address = Address.find(params[:id])
    @address.destroy
    redirect_to addresses_path
  end

  private
  def address_params
    params.require(:address).permit(:address, :city, :user_id)
  end


end
