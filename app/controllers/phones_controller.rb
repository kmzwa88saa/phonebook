class PhonesController < ApplicationController
  load_and_authorize_resource
  before_filter :check_role, :except => [:show, :new, :edit, :delete]
  before_filter :find_params, :only => [:show, :edit, :update, :destroy]

  def index
  end

  def show
  end

  def new
    @phone = Phone.new
  end

  def create
    @phone = Phone.create(phone_params)
  end

  def edit
  end

  def update
    @phone.update_attributes(phone_params)
  end

  def delete
    @phone = Phone.find(params[:phone_id])
  end

  def destroy
    @phone.destroy
  end

  private
  def phone_params
    params.require(:phone).permit(:phone, :user_id)
  end

  def check_role
    if User.find(current_user.id).role == 'admin'
      @phones = Phone.all
    else
      @phones = Phone.accessible_by(current_ability)
    end
  end

  def find_params
    @phone = Phone.find(params[:id])
  end

end
