class Ability
  include CanCan::Ability
  def initialize(user)
    if user.present?
      if user.role == 'admin'
        can :manage, :all
      elsif user.role == 'user'
        can :manage, Phone, :user_id => user.id
        can :manage, Address
      end
      else
        can :read, :all
      end
    end
 end
