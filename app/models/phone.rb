class Phone < ApplicationRecord
  belongs_to :user
  validates :user_id, presence: true
  validates :phone, presence: true, numericality: { only_integer: true }
end
