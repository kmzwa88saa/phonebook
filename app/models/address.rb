class Address < ApplicationRecord
  belongs_to :user
  validates :address , :city, presence: true
  # validates :city , presence: true
end
