Rails.application.routes.draw do
  devise_for :users, controllers: { sessions: 'users/sessions' }
  resources :phones do
    get "delete"
  end
  resources :addresses
  root to: 'home#index'
end
